import React, { useState, useEffect } from "react"

import { socket } from "./ws"

import styled from 'styled-components'

const Container = styled.div`
    width: 300px;
    max-width: 300px;
`

const ContainerBody = styled.div`
    height:350px;
    overflow:scroll;
`


const Socio = styled.div`
    display:flex;
    flex-direction: column;
    margin-bottom:15px;
    position:relative;
`
const Body = styled.div`
    padding-left:15px;
    padding-rigth:15px;
    display:flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`

const Name = styled.p`
    color: #333;
`
const Phone = styled.p`


`
const Email = styled.p`


`
const Enable = styled.div`
    width: 25px;
    height: 25px;
    border-radius: 50%;

    background-color: ${props => props.enable ? 'green' : 'red'};
    opacity:0.7;
    cursor:pointer;

`
const Button = styled.button`
    background-color: #F44336;
    color:white;
    padding: 10px 20px 10px 20px;
    opacity: 0.8;
    border-radius:15px;
    width: 100%;
    height:50px;
    margin-bottom:25px;
`

const Icon = styled.img`
    margin-left:10px;
    width:25px;
    heigth:25px;
    cursor:pointer;
    
`
const FeedBack = styled.div`  
    align-items:center;
    display:flex;
    flex-direction:row;
    
`

    

const App = () => {

    const [data, setData] = useState([
            

    ])

    useEffect(() => {

          socket.on('res:socios:view',({statusCode, data, message})=>{

            console.log('res:socios:view',{statusCode, data, message})

            console.log({statusCode, data ,  message})

            if(statusCode ===200) setData(data)

        })

        
        setTimeout(() => socket.emit('req:socios:view',({ })), 1000)
        

    },[])

    const handleCreate = () =>{
        socket.emit('req:socios:create',{
            name:'jose', phone:123456
        })
    }

    const handleDelete = (id) =>{
        socket.emit('req:socios:delete',{id})
    }

    const handleChange = (id, status) =>{
        if(status) socket.emit('req:socios:disable',{id})
        else socket.emit('req:socios:enable',{id})
    }


    return(
        <Container>
            <Button onClick={handleCreate}>Create</Button>
            <ContainerBody>
                
        
                {
                    data.map((v, i) => (
                        <Socio>


                            <Body>
                                <Name>{v.name} <small>{v.id}</small></Name>
                                <Phone>{v.phone} </Phone>
                            </Body>
                            <Body>
                                <Email>{v.email}</Email>

                                <FeedBack>
                                    <Enable enable={v.enable} onClick={() => handleChange(v.id, v.enable)}/>
                                    <Icon src={"https://cdn-icons-png.flaticon.com/128/1828/1828843.png"}
                                        onClick={ () => handleDelete(v.id)}/>
                                

                                </FeedBack>

                                
                            </Body>

                        </Socio>
                        ))
                }
            </ContainerBody>
        </Container>

    )


}

export default App