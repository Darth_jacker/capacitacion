const {sequelize} = require('../settings')
const {DataTypes}= require('sequelize')

const Model = sequelize.define('curso',{

    name: {type: DataTypes.STRING},

    edad: {type: DataTypes.BIGINT},

    color: {type: DataTypes.STRING},


})

async function SyncDB(){

    try {
        
        console.log('vamos a inicializar bd')

        await Model.sync({logging:false})

        console.log('bd inicializada')

        return {statusCode:200, data: 'ok'}

    } catch (error) {
        console.log(error)

        return {statusCode:500, menssage:error.toString()}
    }

}


module.exports = {SyncDB, Model}