const {sequelize} = require('../settings')
const {DataTypes}= require('sequelize')
const { name } = require('../../package.json')

const Model = sequelize.define(name,{

    socio: {type: DataTypes.BIGINT},

    amount: {type: DataTypes.BIGINT},


})

async function SyncDB(){

    try {
        
        console.log('vamos a inicializar bd')

        await Model.sync({logging:false, force:true})

        console.log('bd inicializada')

        return {statusCode:200, data: 'ok'}

    } catch (error) {
        console.log(error)

        return {statusCode:500, menssage:error.toString()}
    }

}


module.exports = {SyncDB, Model}