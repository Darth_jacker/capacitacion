const Controllers = require('../Controllers')
const { InternalError, redis } = require('../settings')
const socios = require('api-socios')

async function Create({ socio, amount }){
   
    try {

        const validSocio =  await socios.FindOne({ id: socio }, redis)

        if(validSocio.statusCode !== 200){
            switch(validSocio.statusCode){
                case 400:{
                    return{statusCode:400, menssage: "No existe el usario"}
                }

                default: return {statusCode:500, menssage: InternalError}
            }
        }

        

        if(!validSocio.data.enable){
            return {statusCode:400, message: "el socio no se encuentra activo"}
        }

        let {statusCode, data, menssage} = await Controllers.Create({ socio, amount })

        return {statusCode, data, menssage}
        
 
    } catch (error) {
        console.log({step:'service Create', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
     
}

async function Delete({ id }){
   
    try {

        const findOne = await Controllers.FindOne({where:{id}})

        if(findOne.statusCode !== 200) {

            /*let response = {
                400: {statusCode:400, menssage:"no existe el usuario a eliminar"},
                500: {statusCode:500, menssage: InternalError}
            }

            return response[findOne.statusCode]*/

            switch(findOne.statusCode){
                case 400: return {statusCode:400, menssage:"no existe el usuario a eliminar"}
                default: return {statusCode:500, menssage: InternalError}

            }

        }
        
        const del = await Controllers.Delete({where:{ id }})

        if(del.statusCode===200)return {statusCode:200, data:findOne.data}
        
        return {statusCode:400, message:InternalError}
        
 
    } catch (error) {
        console.log({step:'service Delete', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
     
}

async function FindOne({id}){
   
    try {

        let {statusCode, data, menssage} = await Controllers.FindOne({where:{id}})

        return {statusCode, data, menssage}
        
 
    } catch (error) {
        console.log({step:'service FindOne', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
     
}

async function View({}){
   
    try {

        let {statusCode, data, menssage} = await Controllers.View({})

        return {statusCode, data, menssage}
        
 
    } catch (error) {
        console.log({step:'service View', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
     
}

module.exports = {Create, Delete, FindOne, View}