import React, { useState, useEffect } from "react"

import styled from "styled-components";

import { socket } from "./ws"

const Container = styled.div`
    position: relative;
    flex-direction.row;
    flex-wrap:wrap;
    justify-content: space-evenly;
    align-self:center;
    align-content:stretch;
    display:flex;
    width:100%;  
`

const Libro = styled.div`
    flex-direction:column;
    background-color: #303536;
    margin-bottom: 15px;
    border-radius:10px;
    padding: 2px;
    display:flex;
    position:relative;

`

const Portada = styled.img`
    width:200px;
`
const Icon = styled.img`
    width:35px;
    heigth:35px;
    position:absolute;
    top:-6px;
    right:-6px;
    cursor:pointer;

`

const Title = styled.p`
    color: white;
    text-align: center;

`
const Button = styled.button`
    background-color: #036e00;
    color:white;
    padding: 10px 20px 10px 20px;
    opacity: 0.8;
    border-radius:15px;
    width: 100%;
    height:50px;
    margin-bottom:50px;
`

const Item = ({title, image, id}) =>{

    const handleDelete = () =>{
        socket.emit('req:libros:delete',{id})
    }

    const opts ={
        src:"https://cdn-icons-png.flaticon.com/128/1828/1828843.png",
        onClick: handleDelete
    }

    return(
        <Libro>
            <Icon {...opts}/>
            <Portada src={image}/>
            <Title>{title}</Title>

        </Libro>
    )
}

const App = () =>{

    const [data, setData] = useState([])

useEffect(() => {

      socket.on('res:libros:view',({statusCode, data, message})=>{

        console.log('res:libros:view',{statusCode, data, message})

        console.log({statusCode, data ,  message})

        if(statusCode ===200) setData(data)

    })

    
    setTimeout(() => socket.emit('req:libros:view',({ })), 1000)
    

},[])

const handleCreate = () =>{
    socket.emit('req:libros:create',{
        image:"https://nodejs.org/static/images/logo.svg", title:"aprende nodejs"
    })
}


    return(
        <Container>
            <Button onClick={handleCreate}>Create</Button>  
           {
                data.map((v, i) => <Item {...v}/>)
            }
           
        </Container>


    )


}

export default App
