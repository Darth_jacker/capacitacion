const { io } = require('socket.io-client')

const socket = io("http://localhost")

async function main(){
    try {
        
        setTimeout(()=> console.log(socket.id), 500)

        socket.on('res:microservice:view',({statusCode, data, message})=>{

            console.log('res:microservice:view',{statusCode, data, message})

        })

        socket.on('res:microservice:create',({statusCode, data, message})=>{

            console.log('res:microservice:create',{statusCode, data, message})

        })

        socket.on('res:microservice:findone',({statusCode, data, message})=>{

            console.log('res:microservice:findone',{statusCode, data, message})

        })

        socket.on('res:microservice:update',({statusCode, data, message})=>{

            console.log('res:microservice:update',{statusCode, data, message})

        })

        socket.on('res:microservice:delete',({statusCode, data, message})=>{

            console.log('res:microservice:delete',{statusCode, data, message})

        })



        /*setInterval(()=>{
                socket.emit('req:microservice:view',({ }))
        }, 300)*/

        setTimeout(() =>{
           socket.emit('req:microservice:create',({name:'ejemplo', age:80, color:'verde'}))
           // socket.emit('req:microservice:delete',({id:4}))
           //socket.emit('req:microservice:update',({id:4, name:'otra'}))
            //socket.emit('req:microservice:view',({ }))
           // socket.emit('req:microservice:findone',({id:4}))
        }, 300)



    } catch (error) {
        console.log(error)
        
    }

}

main()