import React, { useState, useEffect } from "react"

import { socket } from "./ws"

import styled from 'styled-components'

const Container = styled.div`
    width: 300px;
    max-width: 300px;
`

const ContainerBody = styled.div`
    height:350px;
    overflow:scroll;
`


const Socio = styled.div`
    display:flex;
    flex-direction: column;
    margin-bottom:15px;
    position:relative;
`
const Body = styled.div`
    padding-left:15px;
    padding-rigth:15px;
    display:flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`

const Name = styled.p`
    color: #333;
`


const Button = styled.button`
    background-color: #F44336;
    color:white;
    padding: 10px 20px 10px 20px;
    opacity: 0.8;
    border-radius:15px;
    width: 100%;
    height:50px;
    margin-bottom:25px;
`

const Icon = styled.img`
    margin-left:10px;
    width:25px;
    heigth:25px;
    cursor:pointer;
    
`
const Input = styled.input`
    margin-left:10px;
    width:90%;
    border-radius: 2px;
    heigth:25px;
    cursor:pointer;
    
`


const App = () => {

    const [data, setData] = useState([])
    const [value, setValue] = useState()

    useEffect(() => {

          socket.on('res:pagos:view',({statusCode, data, message})=>{

            console.log('res:pagos:view',{statusCode, data, message})

            console.log({statusCode, data ,  message})

            if(statusCode ===200) setData(data)

        })

        
        setTimeout(() => socket.emit('req:pagos:view',({ })), 1000)
        

    },[])

    const handleCreate = () => value > 0 ? 
        socket.emit('req:pagos:create',{
            socio:value, amount:300
        }) : null

    const handleDelete = (id) =>{
        socket.emit('req:pagos:delete',{id})
    }

    const handleInput = (e) => setValue(e.target.value)
    

    return (
        <Container>

            <Input type={'number'} onChange={handleInput}></Input>

            <Button onClick={handleCreate}>Aplicar pago para el socio {value}</Button>
            <ContainerBody>
                
        
                {
                    data.map((v, i) => (
                        <Socio>

                            


                            <Body>
                                <Name>Cupon de pago: {v.id} </Name>
                                <Name>Socio: {v.socio} </Name>
                            </Body>
                            <Body>
                                <Name>{v.createdAt}</Name>

                                <Icon src={"https://cdn-icons-png.flaticon.com/128/1828/1828843.png"}
                                        onClick={ () => handleDelete(v.id)}/>
                                

                               

                                
                            </Body>

                        </Socio>
                        ))
                }
            </ContainerBody>
        </Container>



    )

}

export default App